import requests
import time

from delphi.models import OracleAnswer, OracleInquiry


def get_answer():
    time.sleep(12)  # emulate long running call
    oracle_url = 'https://yesno.wtf/api/'
    return requests.get(oracle_url).json()


def add_answer_to_inquiry(inquiry_id):
    oracle_response = get_answer()
    oracle_answer = {
        'answer': oracle_response['answer'],
        'answer_image': oracle_response['image'],
    }
    answer = OracleAnswer.objects.create(**oracle_answer)

    inquiry = OracleInquiry.objects.get(id=inquiry_id)
    inquiry.answer = answer
    inquiry.save()
