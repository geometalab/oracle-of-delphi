from delphi.serializers import OracleInquirySerializer, OracleAnswerSerializer, CommentSerializer


def _readonly_fields_in_serializer(fields, *, serializer):
    for field in fields:
        assert field in serializer.fields
        assert serializer.fields[field].read_only


def test_oracle_inquiry_serializer_fields_have_correct_accessability():
    readonly_fields = ['modified', 'created', 'answer']
    serializer = OracleInquirySerializer()

    _readonly_fields_in_serializer(readonly_fields, serializer=serializer)

    assert 'question' in serializer.fields
    question_field = serializer.fields['question']
    assert question_field.read_only is False
    assert question_field.write_only is False
    assert question_field.required is True


def test_oracle_answer_serializer_fields_have_correct_accessability():
    readonly_fields = ['modified', 'created', 'answer', 'answer_image']
    serializer = OracleAnswerSerializer()

    _readonly_fields_in_serializer(readonly_fields, serializer=serializer)


def test_comment_serializer_fields_have_correct_accessability():
    readonly_fields = ['modified', 'created']
    serializer = CommentSerializer()

    _readonly_fields_in_serializer(readonly_fields, serializer=serializer)

    assert 'text' in serializer.fields
    text_field = serializer.fields['text']
    assert text_field.read_only is False and text_field.write_only is False and text_field.required is True

    assert 'username' in serializer.fields
    username_field = serializer.fields['username']
    assert username_field.read_only is False and username_field.write_only is False and username_field.required is True

    assert 'question' in serializer.fields
    question_field = serializer.fields['question']
    assert question_field.read_only is False and question_field.write_only is False and question_field.required is True
