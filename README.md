# Oracle of Delphi Example Application

## Structure

Javascript Frontend Code can be found in the `frontend` folder.

Django/Python Backend Code can be found in the `backend` folder.

## Prerequisites

The entire project runs inside docker, development and production.

You need to have installed 
[Docker](https://www.docker.com/) [1.13.1 or higher] 
and 
[docker-compose](https://docs.docker.com/compose/) [1.11.2 or higher].

## Usage

How this project can be used.

### Using the `Makefile` 

* build images: `make build`
* upgrade requirements: `make requirements`

### Running the project

#### In Development

```shell
make build
docker-compose build
docker-compose up
```

#### In Production

In progress...

```shell
make build
docker-compose -f docker-compose.yml -f docker-compose-prod.yml build
docker-compose -f docker-compose.yml -f docker-compose-prod.yml up -d
```
