from rest_framework import viewsets

from .models import OracleInquiry, Comment, OracleAnswer
from .serializers import OracleInquirySerializer, CommentSerializer, OracleAnswerSerializer


class OracleInquiryViewSet(viewsets.ModelViewSet):
    serializer_class = OracleInquirySerializer
    queryset = OracleInquiry.objects.all()


class OracleAnswerViewSet(viewsets.ReadOnlyModelViewSet):
    serializer_class = OracleAnswerSerializer
    queryset = OracleAnswer.objects.all()


class CommentViewSet(viewsets.ModelViewSet):
    serializer_class = CommentSerializer
    queryset = Comment.objects.all()
